Jean-Bombeur est un vendeur de sandwich à la carte bien connu sur la place locale.

Le principe est de composer et commander son (ses) sandwichs à l'aide d'une application web. Cette
application doit fonctionner sur tous types de terminaux. Il doit être possible de suivre sa commande
(planifiée, en cours de préparation, prête ...)  et éventuellement de la modifier tant qu'elle est en 
attente.



## Pré-requis ##
Vous devez avoir bower installer sur votre ordinateur

## Installation ##

Vous pouvez installer ce projet en executant ces quelques lignes de code:

```
#!bash
git clone https://Iryu54@bitbucket.org/Iryu54/jean-bombeur.git jean-bombeur && cd jean-bombeur && bower install

```





## Execution ##

Il vous suffit d'ouvrir le fichier index.html dans votre navigateur préféré.